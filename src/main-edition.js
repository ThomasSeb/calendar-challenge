import { swapConsole } from "./swap-console";


let button1 = document.querySelector('#buttonEvent');
let button2 = document.querySelector('#buttonLabel');

let affichageEvent = document.querySelector('#affichageEvent');
let affichageLabel = document.querySelector('#affichageLabel');



button1.addEventListener('click', function (event) {
    swapConsole(affichageLabel, affichageEvent);
});

button2.addEventListener('click', function (event) {
    swapConsole(affichageEvent, affichageLabel);
});