export class EventLabel {
    /**
     * 
     * @param {Number} eventLabelId 
     * @param {String} description Short description of label
     * @param {String} color Hexadec Color
     */
    constructor(eventLabelId, description, color) {
        eventLabelId = eventLabelId;
        description = description;
        color = color;
        linkedEvents = [];
    }
    edit(newDescription, newColor) {
        this.description = newDescription;
        this.color = newColor;
    }
    delete() {

    }
}